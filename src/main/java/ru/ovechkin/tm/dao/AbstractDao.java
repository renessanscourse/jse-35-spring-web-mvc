package ru.ovechkin.tm.dao;

import ru.ovechkin.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDao <E extends AbstractEntity> {

    protected final List<E> entities = new ArrayList<>();

    public void add(E e) {
        entities.add(e);
    }

    public List<E> findAll() {
        return entities;
    }

    public void removeById(final String id) {
        entities.removeIf(e -> e.getId().equals(id));
    }

    public E findById(final String id) {
        for (final E e : entities) {
            if (e.getId().equals(id)) return e;
        }
        return null;
    }

    public void update(final String id, final E e) {
        final E entityToUpdate = findById(id);
        entityToUpdate.setName(e.getName());
        entityToUpdate.setDescription(e.getDescription());
    }

}