package ru.ovechkin.tm.dao;

import org.springframework.stereotype.Component;
import ru.ovechkin.tm.model.Project;

@Component
public class ProjectDao extends AbstractDao<Project> {

    {
        final Project project1 = new Project("testProject", "testProjectDescription");
        project1.setId("testId");
        entities.add(project1);

        final Project project2 = new Project("2testProject2", "2testProjectDescription");
        project2.setId("testId2");
        entities.add(project2);
    }

}