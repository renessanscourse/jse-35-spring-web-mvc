package ru.ovechkin.tm.dao;

import org.springframework.stereotype.Component;
import ru.ovechkin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

@Component
public class TaskDao extends AbstractDao<Task> {

    {
        entities.add(new Task("task1", "desc1", "testId"));
        entities.add(new Task("task2", "desc2", "testId"));
        entities.add(new Task("task3", "desc3", "testId"));

        entities.add(new Task("t123ask1", "desc1", "testId2"));
        entities.add(new Task("321task2", "desc2", "testId2"));
        entities.add(new Task("t132ask3", "desc3", "testId2"));
    }

    public List<Task> findTasksByProject(final String projectId) {
        final List<Task> tasks = new ArrayList<>();
        for (final Task task : entities) {
            if (task.getProjectId().equals(projectId)) tasks.add(task);
        }
        return tasks;
    }

    public void removeProjectTasks(final String projectId) {
        final List<Task> tasks = findTasksByProject(projectId);
        entities.removeAll(tasks);
    }

}