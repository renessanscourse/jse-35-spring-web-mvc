package ru.ovechkin.tm.model;

public class Project extends AbstractEntity {

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Project() {
    }

    @Override
    public String toString() {
        return "\n[NAME] = " + name +
                "\n[DESCRIPTION] = " + description +
                "\n[ID] = " + id +
                "\n";
    }
}