package ru.ovechkin.tm.model;

public class Task extends AbstractEntity {

    private String projectId = "";

    public Task(String name, String description, String projectId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
    }

    public Task() {
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "\n[NAME] = " + name +
                "\n[DESCRIPTION] = " + description +
                "\n[ID]= " + id +
                "\n[PROJECT_ID]= " + projectId +
                "\n";
    }

}