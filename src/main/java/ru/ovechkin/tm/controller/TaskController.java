package ru.ovechkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.dao.TaskDao;
import ru.ovechkin.tm.model.Project;
import ru.ovechkin.tm.model.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskDao taskDao;

    @GetMapping("/{projectId}")
    public String show(@PathVariable("projectId") final String projectId, Model model) {
        model.addAttribute("tasks", taskDao.findTasksByProject(projectId));
        return "tasks/allOfProject";
    }

    @GetMapping("/createForm")
    public String getCreateProjectForm(@RequestParam("projectId") String projectId, Model model) {
        model.addAttribute("task", new Task());
        model.addAttribute("projectId", projectId);
        return "tasks/create";
    }

    @PostMapping("/create")
    public String create(
            @RequestParam("projectId") String projectId,
            @ModelAttribute("task") Task task
    ) {
        task.setProjectId(projectId);
        taskDao.add(task);
        String redirect = "redirect:/tasks/" + projectId;
        return redirect;
    }

    @GetMapping("/remove")
    public String remove(
            @RequestParam("taskId") String taskId,
            @RequestParam("projectId") String projectId
    ) {
        taskDao.removeById(taskId);
        String redirect = "redirect:/tasks/" + projectId;
        return redirect;
    }

    @GetMapping("/{id}/editForm")
    public String getEditForm(@PathVariable("id") final String taskId, Model model) {
        model.addAttribute("task", taskDao.findById(taskId));
        return "/tasks/edit";
    }

    @PostMapping("/{id}/edit")
    public String edit(
            @ModelAttribute("task") final Task task,
            @PathVariable("id") final String taskId
    ) {
        taskDao.update(taskId, task);
        String redirect = "redirect:/tasks/" + taskDao.findById(taskId).getProjectId();
        return redirect;
    }

}