package ru.ovechkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.dao.ProjectDao;
import ru.ovechkin.tm.dao.TaskDao;
import ru.ovechkin.tm.model.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private TaskDao taskDao;

    @GetMapping("/all")
    public String allProjects(Model model) {
        model.addAttribute("projects", projectDao.findAll());
        return "projects/all";
    }

    @GetMapping("/createForm")
    public String getCreateProjectForm(Model model) {
        model.addAttribute("project", new Project());
        return "projects/create";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute("project") Project project) {
        projectDao.add(project);
        return "redirect:/projects/all";
    }

    @GetMapping("/remove")
    public String remove(
            @RequestParam("projectId") String projectId
    ) {
        taskDao.removeProjectTasks(projectId);
        projectDao.removeById(projectId);
        return "redirect:/projects/all";
    }

    @GetMapping("/{id}")
    public String getEditForm(@PathVariable("id") final String projectId, Model model) {
        model.addAttribute("project", projectDao.findById(projectId));
        return "/projects/edit";
    }

    @PostMapping("/{id}/edit")
    public String edit(
            @ModelAttribute("project") final Project project,
            @PathVariable("id") final String projectId
    ) {
        projectDao.update(projectId, project);
        return "redirect:/projects/all";
    }

}
