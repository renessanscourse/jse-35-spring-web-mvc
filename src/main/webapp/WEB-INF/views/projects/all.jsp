<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <style type="text/css">
        .header {
            background-color: black;
            color: azure;
            height: 60px;
        }
        .customButton {
            background-color: green;
            color: aliceblue;
            width: 200px;
            height: 30px;
            position: center;
            cursor: pointer;
        }
    </style>

    <head>
        <title>Projects</title>
    </head>

    <header class="header"></header>

    <body>
        <h1>PROJECT MANAGEMENT</h1>

        <table border="3" cols="4" width=100% class="table-view">
            <tr>
                <td width="35%">PROJECT NAME</td>
                <td width="35%">PROJECT DESCRIPTION</td>
                <td width="10%">TASKS OF PROJECT</td>
                <td width="10%">PROJECT EDIT</td>
                <td width="10%">PROJECT REMOVE</td>
            </tr>
            <c:forEach items="${projects}" var="project">
                <tr>
                    <td width=35%>${project.name}</td>
                    <td width=35%>${project.description}</td>
                    <td width="10%"><a href="/tasks/${project.id}">TASKS</a></td>
                    <td width="10%"><a href="/projects/${project.id}">EDIT</a></td>
                    <td width="10%"><a href="/projects/remove?projectId=${project.id}">REMOVE</a></td>
                </tr>
            </c:forEach>
        </table>
        <div><br></div>

        <form action="/projects/createForm">
            <input type="submit" value="CREATE PROJECT" class="customButton">
        </form>

    </body>

</html>