<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>

    <style type="text/css">
        .header {
            background-color: black;
            color: azure;
            height: 60px;
        }
        .customButton {
            background-color: green;
            color: aliceblue;
            width: 200px;
            height: 30px;
            position: center;
            cursor: pointer;
        }
    </style>

    <head>
        <title>Edit Project</title>
    </head>

    <header class="header"></header>

    <body>
        <h1>EDIT PROJECT</h1>
        <form:form method="POST" action="/projects/${project.id}/edit" modelAttribute="project">
            <form:label path="name">Name</form:label>
            <form:input path="name"/>

            <form:label path="description">Description</form:label>
            <form:input path="description"/>

            <input type="submit" value="UPDATE" class="customButton">
        </form:form>
    </body>

</html>
