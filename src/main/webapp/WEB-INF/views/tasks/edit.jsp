<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>

    <style type="text/css">
         .header {
             background-color: black;
                color: azure;
                height: 60px;
         }
         .customButton {
                background-color: green;
                color: aliceblue;
                width: 200px;
                height: 30px;
                position: center;
                cursor: pointer;
         }
    </style>

    <head>
        <title>Edit Project</title>
    </head>

    <header class="header"></header>

    <body>
        <h1>EDIT TASK</h1>

        <form:form method="POST" action="/tasks/${task.id}/edit" modelAttribute="task">
            <form:label path="name">Name</form:label>
            <form:input path="name"/>

            <form:label path="description">Description</form:label>
            <form:input path="description"/>

            <input type="submit" value="UPDATE" class="customButton">
        </form:form>
    </body>

</html>
